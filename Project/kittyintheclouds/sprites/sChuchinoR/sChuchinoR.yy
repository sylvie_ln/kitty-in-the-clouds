{
    "id": "33746b64-3944-43dc-baf9-75616078f0cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChuchinoR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5dd281e3-d205-4e1f-b72e-eb60509cf0ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33746b64-3944-43dc-baf9-75616078f0cf",
            "compositeImage": {
                "id": "b38e12a7-24ff-46ae-ada3-9b93e1d83302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd281e3-d205-4e1f-b72e-eb60509cf0ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1457b61-8318-4a83-9af1-00f422cb6ddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd281e3-d205-4e1f-b72e-eb60509cf0ab",
                    "LayerId": "b79a32dd-e80d-4116-b16c-fe4c23aa910d"
                }
            ]
        },
        {
            "id": "ab57dc66-e6ab-4659-be6b-1222c7b0a7f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33746b64-3944-43dc-baf9-75616078f0cf",
            "compositeImage": {
                "id": "4c59e0ba-bc3e-4655-b53b-ca4c69771261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab57dc66-e6ab-4659-be6b-1222c7b0a7f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b400e8a3-6b68-4571-8396-8c3dbf7e24f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab57dc66-e6ab-4659-be6b-1222c7b0a7f2",
                    "LayerId": "b79a32dd-e80d-4116-b16c-fe4c23aa910d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b79a32dd-e80d-4116-b16c-fe4c23aa910d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33746b64-3944-43dc-baf9-75616078f0cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}