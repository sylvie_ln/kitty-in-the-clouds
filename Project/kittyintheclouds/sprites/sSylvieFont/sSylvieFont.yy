{
    "id": "4f18dfe1-61b3-471e-96da-67e119a776cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSylvieFont",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49a24bf1-4594-4c52-a7d8-f1fadb4f6870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "ae16d7e2-28a2-474d-8f44-2e175a7acb93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49a24bf1-4594-4c52-a7d8-f1fadb4f6870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08732b1d-b084-414b-9f35-73715954f4be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49a24bf1-4594-4c52-a7d8-f1fadb4f6870",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "73fd04cb-664f-43ef-88f9-30a36c6ebe8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "ce5f1d30-27f7-47fe-92d9-8f7b287d0732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73fd04cb-664f-43ef-88f9-30a36c6ebe8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd21266-2fa3-47dd-b62a-19efdc024b31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73fd04cb-664f-43ef-88f9-30a36c6ebe8e",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "6fd3d52e-c987-4d02-8bf8-6c33023d1232",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "054d00da-0737-47e3-b6d7-1762c0f3fbc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fd3d52e-c987-4d02-8bf8-6c33023d1232",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5dbed27-42ea-40d5-b43c-96335c2efc0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fd3d52e-c987-4d02-8bf8-6c33023d1232",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "5b9d28b1-e0ec-45e3-8935-184e731e8e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "3786118e-ffa7-4bd6-9780-d37657ae9ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b9d28b1-e0ec-45e3-8935-184e731e8e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc1d8c00-7bd0-4b12-a2a4-18eac1b3c731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b9d28b1-e0ec-45e3-8935-184e731e8e62",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "2c37c9f6-c701-4eac-88f0-51ce495592d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "eb53b135-421f-42cb-9b92-21aff76aeed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c37c9f6-c701-4eac-88f0-51ce495592d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac4653f8-17c6-4bcb-8ee6-63833a8cb7ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c37c9f6-c701-4eac-88f0-51ce495592d3",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "55d5f765-e936-485c-8c01-2a983929b146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "8328c7bf-1195-4bb6-ae8c-644e599d34d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55d5f765-e936-485c-8c01-2a983929b146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b554052-8966-48da-a379-cbff33949420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55d5f765-e936-485c-8c01-2a983929b146",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "dc9dc220-3a31-4844-95a2-48131b6e0a7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "38075f5b-a20a-4aff-8234-805c29d4d7bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc9dc220-3a31-4844-95a2-48131b6e0a7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f5e57c7-f28e-4bbb-87a7-f9d2211be99f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc9dc220-3a31-4844-95a2-48131b6e0a7b",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "11761882-42f4-44d0-a21e-2e3815dcef89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "5f6d9868-38d1-4f8f-8cd2-cda082ab8fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11761882-42f4-44d0-a21e-2e3815dcef89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba47809-7c60-4f02-b186-53b9c52cef52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11761882-42f4-44d0-a21e-2e3815dcef89",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "34ee6d1f-28f4-42db-86c0-760408a45ff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "30d77876-770a-40e2-b829-5fc1c9dadfbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ee6d1f-28f4-42db-86c0-760408a45ff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474f7f2f-104c-40af-bf52-b2b410d12bd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ee6d1f-28f4-42db-86c0-760408a45ff6",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "d797432f-e798-4399-8d8c-d6518e76c695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "32574da7-85a0-4b66-8c86-599521a7e3c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d797432f-e798-4399-8d8c-d6518e76c695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41408494-f03d-4d86-9720-12acb81f23ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d797432f-e798-4399-8d8c-d6518e76c695",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "18afb387-e771-4e37-8baa-f7e8ebbe640e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "3889f5cd-f77a-48d4-acdd-f2e9bf91ede2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18afb387-e771-4e37-8baa-f7e8ebbe640e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73322c2-d2ae-489c-a1e4-defffa799944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18afb387-e771-4e37-8baa-f7e8ebbe640e",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "5b0680ce-526d-4844-8324-45bbb1e7b742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "0b6038f9-8bfa-45e8-9346-a89da9a67847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b0680ce-526d-4844-8324-45bbb1e7b742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae56081f-02f2-4e4b-a7a0-f571f5b6feac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b0680ce-526d-4844-8324-45bbb1e7b742",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "586c1b27-7c9a-47f6-ba37-031fefa1608f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "1237c296-701c-4e44-9a7f-ad1c8ee12ffb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "586c1b27-7c9a-47f6-ba37-031fefa1608f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d6aa2bd-49c6-445a-98bb-6ba8029f7e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "586c1b27-7c9a-47f6-ba37-031fefa1608f",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "f6dfa5b6-8185-4bd7-8801-6c47b69edb58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "1be1f462-eb99-4e85-aa47-cd91d5319870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6dfa5b6-8185-4bd7-8801-6c47b69edb58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d73ae04-2743-4269-85b8-6d6dbd39efd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6dfa5b6-8185-4bd7-8801-6c47b69edb58",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "07ade4b2-21bb-4026-8de0-922a0b5f9cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "df21d1b2-a092-407e-b34f-34d023a0dd4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07ade4b2-21bb-4026-8de0-922a0b5f9cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49499e05-5f92-447c-861c-a829c26a9dc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07ade4b2-21bb-4026-8de0-922a0b5f9cfd",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "3b581d1b-9b06-4b20-8efe-d299fa5e82f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "e3ae804e-31d5-4005-ad98-34b885f65e7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b581d1b-9b06-4b20-8efe-d299fa5e82f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09e7d8e9-e8d0-4f7d-819c-958acb07acb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b581d1b-9b06-4b20-8efe-d299fa5e82f7",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "b0ac39aa-304f-4d73-bb91-ea699568bce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "6441684c-e740-4a73-91c4-fc6c8606961e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0ac39aa-304f-4d73-bb91-ea699568bce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99658a13-3844-4758-a65c-c3b29b461733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0ac39aa-304f-4d73-bb91-ea699568bce1",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "f0cb537f-827c-4c36-a940-69e5f21a72fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "e3528306-918a-49c1-b099-1e0a3e37c34c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0cb537f-827c-4c36-a940-69e5f21a72fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7de79ff-4b21-436e-a362-ac0176e877c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0cb537f-827c-4c36-a940-69e5f21a72fb",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "edd49fa4-2c07-49c0-ab1b-f5bc11ce339c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "65749526-86e3-46a8-8735-3802bafbe417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edd49fa4-2c07-49c0-ab1b-f5bc11ce339c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c90d762-f2e2-4aaa-8cec-27c1ead6eac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edd49fa4-2c07-49c0-ab1b-f5bc11ce339c",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "0e9fd1dc-8215-44f3-9dea-0798afa0ed11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "bea25382-6721-4fcd-b7b0-a45378da6176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e9fd1dc-8215-44f3-9dea-0798afa0ed11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d707424-64bd-43e3-a558-add421f66ee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e9fd1dc-8215-44f3-9dea-0798afa0ed11",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "97f381fb-46cb-44c8-bd6d-db41be45787f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "ad5392d2-af15-493e-ae83-149b6ee6a4ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97f381fb-46cb-44c8-bd6d-db41be45787f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f79284f-f53e-4be3-b708-9f434d29ebc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97f381fb-46cb-44c8-bd6d-db41be45787f",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "c4cac35c-2899-429a-8557-2de987ff76ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "e818610a-187c-41f4-b613-eb3b73cf9d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4cac35c-2899-429a-8557-2de987ff76ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a2ae57f-b829-4e2a-94b4-20832c7f4b7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4cac35c-2899-429a-8557-2de987ff76ad",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "d960db91-9c12-4024-bde8-b17a3780f634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "552ae199-41cd-4e71-9021-f323b3da2570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d960db91-9c12-4024-bde8-b17a3780f634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b20e6b-37db-43e1-ac35-e04e32387aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d960db91-9c12-4024-bde8-b17a3780f634",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "a7bceb28-b283-45b8-b866-d95434be46e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "0fb6870b-ba52-403f-a617-cc6b55530325",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7bceb28-b283-45b8-b866-d95434be46e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c589f39-d765-4994-a683-97a82106652d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7bceb28-b283-45b8-b866-d95434be46e0",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "2a0dbf81-c86b-429b-8fdb-5caa4c222125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "66e61212-e926-4627-93c1-21d3eb5b7ba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a0dbf81-c86b-429b-8fdb-5caa4c222125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f8c0438-b1af-426e-9ea8-3b2c2f19fa27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0dbf81-c86b-429b-8fdb-5caa4c222125",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "d4e209d7-7ae1-4c29-8943-f6267d8c5498",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "687e6798-3d64-4a22-9907-e6cdadf9558a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e209d7-7ae1-4c29-8943-f6267d8c5498",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b92d8799-c364-4922-8c87-d8a27fceabca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e209d7-7ae1-4c29-8943-f6267d8c5498",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "1353e619-6ccb-4d34-9bca-d74921eb871a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "a3ae060b-a4d3-40ac-a832-9310745edc6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1353e619-6ccb-4d34-9bca-d74921eb871a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85e9729e-4948-4681-90a5-9b50df8a4d7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1353e619-6ccb-4d34-9bca-d74921eb871a",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "d06111bf-e7df-4dbe-a2c7-2d821600385c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "dd7c5c35-19ca-4124-8a3e-0142cd9f103a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d06111bf-e7df-4dbe-a2c7-2d821600385c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead4d86a-af9b-407d-bfcb-f15758a469ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d06111bf-e7df-4dbe-a2c7-2d821600385c",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "1914fa88-dcb6-4d64-a6c5-619c47832c36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "37902ee6-2885-47ac-b1eb-620d5cd57827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1914fa88-dcb6-4d64-a6c5-619c47832c36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4abb2e18-559f-4ed5-ac96-8536742e354f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1914fa88-dcb6-4d64-a6c5-619c47832c36",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "7c4f8371-ec3c-49cc-b623-eefb59a15958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "a1b397bc-018a-4723-98c0-ae660dc82f63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c4f8371-ec3c-49cc-b623-eefb59a15958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47bfe708-41b2-40d4-a027-5eb26155cfeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c4f8371-ec3c-49cc-b623-eefb59a15958",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "ffb3e339-2078-4b17-98cc-f90122a83db8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "9c32e664-6cbb-4468-a814-0f61de3aa6cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb3e339-2078-4b17-98cc-f90122a83db8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a7cafd8-54dd-4881-ac36-a0601e95dde3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb3e339-2078-4b17-98cc-f90122a83db8",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "990e6538-9a09-444c-9345-e03f2dd74169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "7fdc266a-929b-4286-a8a0-cb19df005ca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "990e6538-9a09-444c-9345-e03f2dd74169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "803dcfc6-21c1-4d13-8379-b04e0be4b5af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "990e6538-9a09-444c-9345-e03f2dd74169",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "f113357d-250e-4b20-a8e9-3e29e1b7bc57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "6f1e3484-7398-40ef-851f-da5614cb92c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f113357d-250e-4b20-a8e9-3e29e1b7bc57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d02a9c-e956-45b4-9383-cfb418685829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f113357d-250e-4b20-a8e9-3e29e1b7bc57",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "b9687b53-4a84-48c3-a402-d85253ad0f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "1dee953b-5fd0-4d5a-90c0-46e4e281dab2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9687b53-4a84-48c3-a402-d85253ad0f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d889a15-720d-4893-b295-8b28d6d12f76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9687b53-4a84-48c3-a402-d85253ad0f20",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "7c65a6e0-57b5-4682-954e-09b95eb4a716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "499ddccd-e2d4-446d-9ebf-0513cbac708c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c65a6e0-57b5-4682-954e-09b95eb4a716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f3f904d-f474-4d7e-a11b-54287b1f1d34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c65a6e0-57b5-4682-954e-09b95eb4a716",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "d63d600c-be40-40cf-a137-11bdd45cd2e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "552e01f1-0dd9-4997-a02e-f299069e56f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63d600c-be40-40cf-a137-11bdd45cd2e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea3dd4d-4985-410e-9454-0bf8ed615a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63d600c-be40-40cf-a137-11bdd45cd2e7",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "81d62b49-578d-4f4a-a37a-b96ab3f84646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "2e5975a4-94fc-423e-81ab-ecb814bfeb07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d62b49-578d-4f4a-a37a-b96ab3f84646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a5e3e24-aec3-42b6-b711-8cc7b1e895ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d62b49-578d-4f4a-a37a-b96ab3f84646",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "09ffe1b5-069e-49ef-92db-180074858965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "c761aede-3fa2-42df-ba82-bc82fee582c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ffe1b5-069e-49ef-92db-180074858965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1def63b2-c0ef-4b87-8e86-0b43dce315ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ffe1b5-069e-49ef-92db-180074858965",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "cfb0ea26-3746-4206-9c22-e20a3a7c45ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "12b1d274-a941-47ef-953f-334be4c414c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb0ea26-3746-4206-9c22-e20a3a7c45ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98f05054-0db3-41d9-a39d-5002f257c0ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb0ea26-3746-4206-9c22-e20a3a7c45ff",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "27c36910-a908-438a-a180-38b2ad7e075d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "d29da821-0c20-47b0-b326-f0587cc0eb45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27c36910-a908-438a-a180-38b2ad7e075d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b448dde4-be71-42a5-918d-db521516f784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27c36910-a908-438a-a180-38b2ad7e075d",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "698f60db-9d3b-4764-a8aa-aa34b61b9c5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "7e4c32a1-480b-4b14-9e69-8934bce91fc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698f60db-9d3b-4764-a8aa-aa34b61b9c5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa50f817-d643-417c-b340-e31d492bfbcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698f60db-9d3b-4764-a8aa-aa34b61b9c5f",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "4b848564-2da9-4d44-bf13-6d744d05e18e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "8f822c97-8224-4910-b631-454a247096e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b848564-2da9-4d44-bf13-6d744d05e18e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f5ed3c6-f770-4c3b-94b1-9d7840728253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b848564-2da9-4d44-bf13-6d744d05e18e",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "303223c9-ad65-4048-928b-fe8d9d46a0ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "daaeb799-66a7-4012-b2ed-d02667cfb698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "303223c9-ad65-4048-928b-fe8d9d46a0ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d141c55-34d5-4498-8096-ab583da014e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "303223c9-ad65-4048-928b-fe8d9d46a0ff",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "e60fffa6-134c-43b8-9c73-05a659dc39fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "023c3dc0-6662-4989-9a97-e8d31d6949e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e60fffa6-134c-43b8-9c73-05a659dc39fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be491806-99e5-4e15-96fc-d9b5490fcaf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e60fffa6-134c-43b8-9c73-05a659dc39fe",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "330239df-561c-43ff-84b9-335b13b89129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "9e50b684-aa0b-48e4-b051-f28856d851e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330239df-561c-43ff-84b9-335b13b89129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d14814c-01cf-43fe-8fce-743ca39b24c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330239df-561c-43ff-84b9-335b13b89129",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "c55765a5-eccf-43e1-9d27-2c4c0ecce0d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "1987477b-7e68-47bb-9bdc-2bc3d772df97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c55765a5-eccf-43e1-9d27-2c4c0ecce0d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27f933e8-094a-42fc-9cd3-a68c01b319a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c55765a5-eccf-43e1-9d27-2c4c0ecce0d1",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "a4c89887-f5a3-4926-ba89-39d3e62da105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "49b8bcfb-5f0b-4ca5-b573-103800e6f355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c89887-f5a3-4926-ba89-39d3e62da105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657e6777-e445-437f-87cd-c2bd38756ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c89887-f5a3-4926-ba89-39d3e62da105",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "77879de9-4e71-4279-baac-8661e371f4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "6ffbd109-f08b-43ed-a78d-9c982de92c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77879de9-4e71-4279-baac-8661e371f4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c8815d-33ca-4729-ae9c-ba1a30b57c86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77879de9-4e71-4279-baac-8661e371f4c4",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "c36235bd-a381-4562-8eba-9c470dbad1b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "a80bb5c7-8844-4b91-8dea-d5294cd57015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c36235bd-a381-4562-8eba-9c470dbad1b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26f7789f-1e26-43db-8cc1-891607f49b10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c36235bd-a381-4562-8eba-9c470dbad1b8",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "c5927e5a-9b13-47dd-b25b-09f8a3ee485f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "7bd656c1-f93d-480b-a9fb-6c977126c299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5927e5a-9b13-47dd-b25b-09f8a3ee485f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e546ea8-66f9-4d37-b528-4ccf81b529c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5927e5a-9b13-47dd-b25b-09f8a3ee485f",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "960e4660-b058-429e-b8fa-a8be06e921eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "f1030aff-df3f-4601-8e1c-2d4ebdb8c5bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960e4660-b058-429e-b8fa-a8be06e921eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c37b4fb4-284d-46d3-9b6c-802654ad4a1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960e4660-b058-429e-b8fa-a8be06e921eb",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "11907a9b-9f38-4727-ba2b-663887e693fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "66201a40-926e-4871-a2ae-7ea1aa494925",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11907a9b-9f38-4727-ba2b-663887e693fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bb6bcb2-a1cb-4e39-ae6a-82cfcf9bbdfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11907a9b-9f38-4727-ba2b-663887e693fb",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "6a3547cb-bebe-46d2-8165-d55505a394ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "b37a00db-5dde-4ccf-8a5d-152acfa6bf04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3547cb-bebe-46d2-8165-d55505a394ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f35001f-dc72-4c18-83b1-60ac9f9f6dc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3547cb-bebe-46d2-8165-d55505a394ee",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "0916e429-b48f-4794-b483-6a847d1d722e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "5a335890-7ec2-4f25-88c1-7a3be6f0ef51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0916e429-b48f-4794-b483-6a847d1d722e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0b52c3-380d-403f-917a-4ec85edc7cc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0916e429-b48f-4794-b483-6a847d1d722e",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "d6df78ee-59be-4358-893e-ba93991e0abe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "d29bfae5-e4bd-4786-b9cf-ec31454d1a20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6df78ee-59be-4358-893e-ba93991e0abe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58eea9cf-3dbb-46dd-bbfe-cd8b9521d145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6df78ee-59be-4358-893e-ba93991e0abe",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "de2e9189-d94b-41de-9b72-39f56fe278f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "ed513ea4-92f3-4fbb-b95d-b225f76e7047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de2e9189-d94b-41de-9b72-39f56fe278f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "588396be-91a4-4ea9-bde3-60d254c9336d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de2e9189-d94b-41de-9b72-39f56fe278f0",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "c230a287-e46d-41c5-871f-a366fdba76ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "8600464d-aa4c-49ff-8320-b15649d1a302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c230a287-e46d-41c5-871f-a366fdba76ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd8271af-0055-4113-a883-b3eb460dba6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c230a287-e46d-41c5-871f-a366fdba76ba",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "dffb6685-6015-4de9-95ac-d006171d63f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "0611318b-7e73-4fdf-9144-82a3f777338d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dffb6685-6015-4de9-95ac-d006171d63f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6abf5bad-fd1f-4e61-9b4f-5e98d1e4c1ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dffb6685-6015-4de9-95ac-d006171d63f8",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "235b778b-fb79-466d-bd72-340a915445f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "bdb19c10-95bc-46f6-9fdf-c1be972f5761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "235b778b-fb79-466d-bd72-340a915445f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e887b4a-9014-4402-93b0-f6a9618cf165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "235b778b-fb79-466d-bd72-340a915445f1",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "ef83760d-b5b3-40e9-9404-4093ddcf8eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "69d8d98f-343b-4877-9b57-23dd6dc3768f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef83760d-b5b3-40e9-9404-4093ddcf8eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c4a3fa4-2d05-4bf4-8aac-200af180ff08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef83760d-b5b3-40e9-9404-4093ddcf8eba",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "b0252616-105a-434e-bd78-66003fabeaac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "cb584e75-1b05-4a6c-bb0d-a10aa488ced5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0252616-105a-434e-bd78-66003fabeaac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7347068-f35f-4108-be49-117d1235338a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0252616-105a-434e-bd78-66003fabeaac",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "8d96af2a-cd6c-46c2-befb-db3037f671ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "965b1c8d-4847-4197-a6c7-cc2ce5af863e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d96af2a-cd6c-46c2-befb-db3037f671ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c2d87d1-8e32-4161-97ce-36c027a96549",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d96af2a-cd6c-46c2-befb-db3037f671ef",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "f8bf81da-569b-4cfa-b655-93c41033188d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "7bc88a0a-3383-4c29-9249-44837032eb61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8bf81da-569b-4cfa-b655-93c41033188d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "347c73cc-3a73-4537-a50c-19c7868c9388",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8bf81da-569b-4cfa-b655-93c41033188d",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "f01eb957-8272-4b25-b6e4-9942b43b44af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "e6cc7fde-8408-4339-99c4-863d2070fc87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f01eb957-8272-4b25-b6e4-9942b43b44af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb03e073-e306-4c3a-a45a-07880450f115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f01eb957-8272-4b25-b6e4-9942b43b44af",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "3dd1ee01-3986-457c-9682-2e0ebd9fd053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "f3c1c081-bec6-4cf4-af1f-4fca143a8392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dd1ee01-3986-457c-9682-2e0ebd9fd053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a95680-c5b7-4c44-8ee5-70cf59cff85a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dd1ee01-3986-457c-9682-2e0ebd9fd053",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "30d6774f-3c6d-4f23-91e3-76b701b6da12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "0e81f5ba-0347-417e-98d8-6d69d755bb74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30d6774f-3c6d-4f23-91e3-76b701b6da12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eebc85c-8e8d-4a0b-a826-01bb5a6bb6a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30d6774f-3c6d-4f23-91e3-76b701b6da12",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "aab2fc55-cb56-4123-aa85-99ce7b81a52e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "05ba1da2-bce2-4149-8dcb-aac03380b8ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab2fc55-cb56-4123-aa85-99ce7b81a52e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48024a41-c413-4f48-ae95-ca9189e174a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab2fc55-cb56-4123-aa85-99ce7b81a52e",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "3eabdf7e-3513-4c87-a54a-2663901168f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "0a8ac409-c81e-4a57-9f82-be87bcecc8fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eabdf7e-3513-4c87-a54a-2663901168f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a935f1c-2ae4-4001-84ba-9388bcb428db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eabdf7e-3513-4c87-a54a-2663901168f4",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "a26512bd-0784-4f0f-850f-32dd7027d5be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "0158e07a-d35a-4eaf-bed0-97c8eb136af2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a26512bd-0784-4f0f-850f-32dd7027d5be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "962f669e-e0b5-44e7-b125-94f4b622b755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a26512bd-0784-4f0f-850f-32dd7027d5be",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "1292a4bd-6b99-442c-908a-9d016dfffc37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "ff205722-94bf-46b7-83ce-485696a67a0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1292a4bd-6b99-442c-908a-9d016dfffc37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e9c64c4-66a1-459b-84bc-7d4d55a7439c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1292a4bd-6b99-442c-908a-9d016dfffc37",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "750dee08-431a-43dd-ba35-0ee4cee7e129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "bb24347e-1024-4e0a-8f3e-568e9ec86f9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750dee08-431a-43dd-ba35-0ee4cee7e129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a3a2eb8-8336-4961-b0b2-5aa016bae20a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750dee08-431a-43dd-ba35-0ee4cee7e129",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "41bbb065-b871-4c08-b252-1cf9c8af6f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "c43052e3-f06d-468b-b9fe-dc0b306a5b08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41bbb065-b871-4c08-b252-1cf9c8af6f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "959d5d98-6abb-4ca7-83bd-a81272b54e9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41bbb065-b871-4c08-b252-1cf9c8af6f77",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "a012a575-608c-4734-abed-d4e0169f9ace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "c0945b2c-b7d0-4ca0-a6fb-ba360e956e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a012a575-608c-4734-abed-d4e0169f9ace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c61e1f7c-6aef-4c57-a05e-9271e029bbc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a012a575-608c-4734-abed-d4e0169f9ace",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "8687ef7e-b648-44cd-acfa-f39c2666edcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "453314c5-7baf-4efc-9b47-280316d4411f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8687ef7e-b648-44cd-acfa-f39c2666edcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dcc2305-a49b-4692-9b52-9ce6936a2c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8687ef7e-b648-44cd-acfa-f39c2666edcb",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "7fba93d4-99e0-42fb-9ba5-006ab195c2c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "09b3de8c-bc91-42f1-a0be-5093676a4e26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fba93d4-99e0-42fb-9ba5-006ab195c2c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ecae748-498f-4402-b36b-3511f7eeee33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fba93d4-99e0-42fb-9ba5-006ab195c2c4",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "a11900de-e11f-48ca-aae4-85f98706f4b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "ca290be4-0e3b-4e26-99f5-df8abc34bca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a11900de-e11f-48ca-aae4-85f98706f4b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "332e2ed5-8524-40ab-8ab1-9ec81a93857c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a11900de-e11f-48ca-aae4-85f98706f4b7",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "e37d1253-4a52-4ec0-af83-a63766edc639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "f820f8e0-57ab-4601-a5ff-2135b5f301c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e37d1253-4a52-4ec0-af83-a63766edc639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aec12de-17e5-46f6-9fe8-3030d5dee78c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e37d1253-4a52-4ec0-af83-a63766edc639",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "38075df8-7e6d-4653-8e0b-ca004491aa22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "97f82efa-f050-4de6-a498-caa995ddf26d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38075df8-7e6d-4653-8e0b-ca004491aa22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d2002c2-48ba-4f2a-9c81-8c270ffd32e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38075df8-7e6d-4653-8e0b-ca004491aa22",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "df125c14-3735-4010-b364-cd5006a87a65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "fc5bb958-78bc-4cba-b8ed-3a5284d713d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df125c14-3735-4010-b364-cd5006a87a65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c83fd76f-cb70-4a13-8749-7bfe6a0ca207",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df125c14-3735-4010-b364-cd5006a87a65",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "7853efaf-7052-43f4-a1d8-bb47f5bc7d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "9e414542-db7c-42fb-92a1-63ab6af1b729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7853efaf-7052-43f4-a1d8-bb47f5bc7d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60129adf-3c6c-4a24-8df9-9288f25441f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7853efaf-7052-43f4-a1d8-bb47f5bc7d3b",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "38d2d77b-d1f8-4ba9-b72c-017b858f7a71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "412be820-4977-41b4-8d4c-b63fc43897c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d2d77b-d1f8-4ba9-b72c-017b858f7a71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35f8f206-2924-4c81-8e15-194b4df7f39c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d2d77b-d1f8-4ba9-b72c-017b858f7a71",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "0b11c9cd-5723-47ad-ad85-fddeff5f459e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "473856fa-8c7e-42f3-ad09-4e983ff74cdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b11c9cd-5723-47ad-ad85-fddeff5f459e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c1c02a-17c7-4da8-8cba-ae0426a92deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b11c9cd-5723-47ad-ad85-fddeff5f459e",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "b8ac26e7-2458-4ebb-b43a-5502a7200416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "09f5d491-5f50-4f3f-b262-1304c491ad11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8ac26e7-2458-4ebb-b43a-5502a7200416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bd4ca6d-01dc-4da1-ad31-19742ac4d1dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8ac26e7-2458-4ebb-b43a-5502a7200416",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "a28fed5f-7860-4eb6-9a72-0bba4775a189",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "1c3b0faa-c146-4a9c-9611-d3767050bc09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a28fed5f-7860-4eb6-9a72-0bba4775a189",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a1a6382-7c1a-4e27-988a-572481c696ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a28fed5f-7860-4eb6-9a72-0bba4775a189",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "71012d7e-acdf-48f6-a7aa-ebb62ec89101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "ea6a83c5-7b6e-4ec5-aeca-7246b1207834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71012d7e-acdf-48f6-a7aa-ebb62ec89101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fccce280-ce42-4a4b-a004-d85de6fd73a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71012d7e-acdf-48f6-a7aa-ebb62ec89101",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "f8d27378-861d-4120-af1a-59993355cdda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "34efa916-88b5-4231-9d05-fef8a4663e87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8d27378-861d-4120-af1a-59993355cdda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e720de16-d295-44fd-bb20-d98f2e6b08d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8d27378-861d-4120-af1a-59993355cdda",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "5b997eeb-2fbb-4560-acdb-409589bdf77a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "a816cb87-f178-4dc5-8009-3e1f06284706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b997eeb-2fbb-4560-acdb-409589bdf77a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1056ef41-a9f9-4d91-8d6b-f5b90b572ec4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b997eeb-2fbb-4560-acdb-409589bdf77a",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "573cc709-6a17-4aea-b538-a9a5acd73209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "02b4edc9-c3e3-46db-971d-394c886c24d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573cc709-6a17-4aea-b538-a9a5acd73209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38705cad-ff06-4d64-87eb-8787cf155a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573cc709-6a17-4aea-b538-a9a5acd73209",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "48b22908-4bfa-4165-ba74-5653331158b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "4f738867-206d-4e2a-8c4f-c422b525f343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48b22908-4bfa-4165-ba74-5653331158b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f218ea10-27e0-428b-b072-8c7190175b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b22908-4bfa-4165-ba74-5653331158b5",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "e439b2c1-b3e7-4b36-a4b4-b7e7d7e814da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "32a7b0fa-56dc-4339-ae6d-8fbc8aa58604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e439b2c1-b3e7-4b36-a4b4-b7e7d7e814da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4d810cd-0d54-4a55-96b2-0bdbbc9bd11a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e439b2c1-b3e7-4b36-a4b4-b7e7d7e814da",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "7475f018-c6a1-417d-b131-a8594c83d300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "c805a614-e562-4ad2-8d47-fc0948d5dbfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7475f018-c6a1-417d-b131-a8594c83d300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55cb848b-64da-484e-9b61-b94004ffbde7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7475f018-c6a1-417d-b131-a8594c83d300",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "cd491aff-74f0-4efb-9022-c354562f9603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "0f2f8e6a-14f3-48e9-8ed9-75b48e7efb43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd491aff-74f0-4efb-9022-c354562f9603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2836977-edcd-46f4-89ac-3a5d2d3c6837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd491aff-74f0-4efb-9022-c354562f9603",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "8f300d57-699c-4a2f-9c45-6a3e087dc735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "2f5358a1-c114-4e6c-ac36-638c252d3c15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f300d57-699c-4a2f-9c45-6a3e087dc735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75f2e319-583b-4fbd-b96e-e54b23787070",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f300d57-699c-4a2f-9c45-6a3e087dc735",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "2e4477dc-534d-4d10-b6dd-21d8fa9ca95f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "9da4ad28-7933-411b-a0e8-7f3844b59c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e4477dc-534d-4d10-b6dd-21d8fa9ca95f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4105ab64-6d77-47c6-b754-9251437f5533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e4477dc-534d-4d10-b6dd-21d8fa9ca95f",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        },
        {
            "id": "4a06c3a7-1de7-46fe-a3ba-b54ffb568ed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "compositeImage": {
                "id": "17041ef9-8870-4a4e-b431-9ccf20c962ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a06c3a7-1de7-46fe-a3ba-b54ffb568ed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "910ce3ae-f32b-4d9f-b206-89abb1341116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a06c3a7-1de7-46fe-a3ba-b54ffb568ed0",
                    "LayerId": "7d4a1183-8b5f-41b3-b823-4cef6b44387e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "7d4a1183-8b5f-41b3-b823-4cef6b44387e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f18dfe1-61b3-471e-96da-67e119a776cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}