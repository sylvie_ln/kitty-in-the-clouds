{
    "id": "ba28cd70-494c-4047-b220-cc72492fd91b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBallonShine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 5,
    "bbox_right": 6,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29ae5468-c17b-439d-9c3c-a71eb873a8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba28cd70-494c-4047-b220-cc72492fd91b",
            "compositeImage": {
                "id": "54262fc3-c077-41d2-883e-11f8f9f628a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ae5468-c17b-439d-9c3c-a71eb873a8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec755257-a95c-4662-80d1-4e5d9c656f4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ae5468-c17b-439d-9c3c-a71eb873a8d4",
                    "LayerId": "b47c37a7-4f00-49ed-991c-16faf08806a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b47c37a7-4f00-49ed-991c-16faf08806a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba28cd70-494c-4047-b220-cc72492fd91b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}