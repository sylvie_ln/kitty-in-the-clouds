{
    "id": "4655f694-dc9c-43a5-a4fc-98dd8518c1ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb5c4042-a00d-4e82-a175-dc8215105571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4655f694-dc9c-43a5-a4fc-98dd8518c1ec",
            "compositeImage": {
                "id": "71f631f5-71ab-4e6e-b753-1a10228fd28a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb5c4042-a00d-4e82-a175-dc8215105571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5735747f-2f5a-41a1-9daf-43112a30627a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb5c4042-a00d-4e82-a175-dc8215105571",
                    "LayerId": "40543ae7-6196-438f-b4a3-5c58efe79537"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "40543ae7-6196-438f-b4a3-5c58efe79537",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4655f694-dc9c-43a5-a4fc-98dd8518c1ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}