{
    "id": "50899a89-ac50-4a7f-b4c1-514d4aaad84d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChuPhoto",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f515d569-8f8e-4c2b-b84a-9c2184bb9647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50899a89-ac50-4a7f-b4c1-514d4aaad84d",
            "compositeImage": {
                "id": "3566b08d-3d7d-4b38-877f-34a4be836f71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f515d569-8f8e-4c2b-b84a-9c2184bb9647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b473b98-2624-459c-bac4-e0ab796139e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f515d569-8f8e-4c2b-b84a-9c2184bb9647",
                    "LayerId": "fd861d4e-109b-4c7d-8fb2-611bd55a6ad5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "fd861d4e-109b-4c7d-8fb2-611bd55a6ad5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50899a89-ac50-4a7f-b4c1-514d4aaad84d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 48
}