{
    "id": "334a5f21-f5ca-4625-ae02-55d12d829cc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChuPhotoBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a55a8e5-96d5-467f-aa0f-d456d9a50f99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "334a5f21-f5ca-4625-ae02-55d12d829cc3",
            "compositeImage": {
                "id": "78117e43-5f54-461a-8993-bb248850016f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a55a8e5-96d5-467f-aa0f-d456d9a50f99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0daf830d-6823-464b-a55d-7fd519b76a99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a55a8e5-96d5-467f-aa0f-d456d9a50f99",
                    "LayerId": "f8e51869-c8c5-4a3f-b50d-373965244981"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "f8e51869-c8c5-4a3f-b50d-373965244981",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "334a5f21-f5ca-4625-ae02-55d12d829cc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 384
}