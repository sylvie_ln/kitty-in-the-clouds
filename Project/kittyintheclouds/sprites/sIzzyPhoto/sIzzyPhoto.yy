{
    "id": "3e06028a-2f77-41d8-bda2-59ec84120223",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIzzyPhoto",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7950167-6029-4aca-be2a-6f4c5b2850c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e06028a-2f77-41d8-bda2-59ec84120223",
            "compositeImage": {
                "id": "dfb308fa-a9a9-4b96-99eb-dbf04ca026a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7950167-6029-4aca-be2a-6f4c5b2850c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f2efac3-7050-4479-901a-88800f73fda4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7950167-6029-4aca-be2a-6f4c5b2850c1",
                    "LayerId": "40394114-3b5e-4776-a09f-fbf9c71313c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "40394114-3b5e-4776-a09f-fbf9c71313c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e06028a-2f77-41d8-bda2-59ec84120223",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 48
}