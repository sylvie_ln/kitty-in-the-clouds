{
    "id": "0ed9edf0-4582-4628-8b24-8517d992c9a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIzzyR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28a21d46-e346-41f7-8e65-b97f3253021c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed9edf0-4582-4628-8b24-8517d992c9a1",
            "compositeImage": {
                "id": "1423538e-2114-452f-b879-de9fa9f5be28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28a21d46-e346-41f7-8e65-b97f3253021c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a319cf-6653-4fa4-8601-26f0c7ed661a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a21d46-e346-41f7-8e65-b97f3253021c",
                    "LayerId": "f1e7712f-9c1d-444b-949f-fa4f7b4f321c"
                }
            ]
        },
        {
            "id": "471abe13-c81a-4032-a486-17e534c57fb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed9edf0-4582-4628-8b24-8517d992c9a1",
            "compositeImage": {
                "id": "f498387d-6d90-4626-8332-36641f338663",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "471abe13-c81a-4032-a486-17e534c57fb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1566031-5556-41cc-b824-c644e3322075",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "471abe13-c81a-4032-a486-17e534c57fb1",
                    "LayerId": "f1e7712f-9c1d-444b-949f-fa4f7b4f321c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f1e7712f-9c1d-444b-949f-fa4f7b4f321c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ed9edf0-4582-4628-8b24-8517d992c9a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}