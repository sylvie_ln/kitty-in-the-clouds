{
    "id": "fb751340-d037-4505-995a-6b28fbce491d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChuchino",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e170758-2a40-481a-a163-eaff7ecf15b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb751340-d037-4505-995a-6b28fbce491d",
            "compositeImage": {
                "id": "6bdbe389-c5d2-4ddd-abbf-b5f67740dbd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e170758-2a40-481a-a163-eaff7ecf15b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6526e3-c0c2-4f92-b84d-db684d10ad9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e170758-2a40-481a-a163-eaff7ecf15b6",
                    "LayerId": "158740cb-9a5b-4978-a693-6336f4e4f345"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "158740cb-9a5b-4978-a693-6336f4e4f345",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb751340-d037-4505-995a-6b28fbce491d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}