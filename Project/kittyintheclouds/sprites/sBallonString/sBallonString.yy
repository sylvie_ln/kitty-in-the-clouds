{
    "id": "ed5564af-6d8b-45ef-b397-f5f42cf36ad5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBallonString",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 7,
    "bbox_right": 8,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f0604a0-d4c1-422d-9773-7b19f6b33ad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed5564af-6d8b-45ef-b397-f5f42cf36ad5",
            "compositeImage": {
                "id": "37ad80c6-fbd5-42b3-a294-4c6a3269f86b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f0604a0-d4c1-422d-9773-7b19f6b33ad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94edf8d-500f-4280-86b5-d40626b39b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f0604a0-d4c1-422d-9773-7b19f6b33ad4",
                    "LayerId": "e44cbe25-31ad-4567-bac4-7b5ee74b399e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e44cbe25-31ad-4567-bac4-7b5ee74b399e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed5564af-6d8b-45ef-b397-f5f42cf36ad5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}