{
    "id": "01b5157e-72c5-4cc6-ab7b-a683ec43490f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBallon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee1ad0e2-8082-419a-a0fa-0d67324a920d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01b5157e-72c5-4cc6-ab7b-a683ec43490f",
            "compositeImage": {
                "id": "bc695af0-cbe1-418e-8f41-10feae41574a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee1ad0e2-8082-419a-a0fa-0d67324a920d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f0199a-7215-4396-832f-59478ece06b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee1ad0e2-8082-419a-a0fa-0d67324a920d",
                    "LayerId": "4223f8d7-619e-44bc-ac06-d8f756585681"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4223f8d7-619e-44bc-ac06-d8f756585681",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01b5157e-72c5-4cc6-ab7b-a683ec43490f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}