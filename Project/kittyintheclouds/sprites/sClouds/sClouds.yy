{
    "id": "eda09010-60f2-4935-a9b9-c49b016bdd4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClouds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71990a6c-d6b4-400c-9555-995b435744aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eda09010-60f2-4935-a9b9-c49b016bdd4f",
            "compositeImage": {
                "id": "d3458fbf-e584-4d9e-af91-e8e526b7f6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71990a6c-d6b4-400c-9555-995b435744aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3de6ce5f-f0c8-4a97-8a58-bf3588b5c4ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71990a6c-d6b4-400c-9555-995b435744aa",
                    "LayerId": "12c3eae5-019b-4440-ab82-b8584acc24fd"
                },
                {
                    "id": "097c254a-26fe-44c6-a73f-f5e22cbe8e1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71990a6c-d6b4-400c-9555-995b435744aa",
                    "LayerId": "782c4dca-13ad-4f7f-815e-06099e78b6d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "12c3eae5-019b-4440-ab82-b8584acc24fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eda09010-60f2-4935-a9b9-c49b016bdd4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "782c4dca-13ad-4f7f-815e-06099e78b6d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eda09010-60f2-4935-a9b9-c49b016bdd4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}