{
    "id": "994ccc86-9868-4334-b9b8-64728e43b7a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadLStick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 14,
    "bbox_right": 23,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54b6874f-8cd0-48e1-8eb4-68c4eac15099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "994ccc86-9868-4334-b9b8-64728e43b7a8",
            "compositeImage": {
                "id": "9d283745-ffde-48e7-85df-973adf89c321",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b6874f-8cd0-48e1-8eb4-68c4eac15099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "899a0ca8-1bf5-4ea6-b1f9-36ede3e4bed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b6874f-8cd0-48e1-8eb4-68c4eac15099",
                    "LayerId": "440546d8-fef3-4e8c-98a6-29bb65a0b3ce"
                },
                {
                    "id": "6bca0d4d-e019-4a5e-a5db-d9f02f884e93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b6874f-8cd0-48e1-8eb4-68c4eac15099",
                    "LayerId": "0912e0ed-15fe-44d2-af99-49c34ece8cf3"
                }
            ]
        },
        {
            "id": "2518d6ce-fdf5-44ab-8e35-457c2ab391c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "994ccc86-9868-4334-b9b8-64728e43b7a8",
            "compositeImage": {
                "id": "fb5d17a9-5c50-4365-9161-225620f86265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2518d6ce-fdf5-44ab-8e35-457c2ab391c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3559c02c-89ff-45ab-b438-bff21809fbe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2518d6ce-fdf5-44ab-8e35-457c2ab391c1",
                    "LayerId": "440546d8-fef3-4e8c-98a6-29bb65a0b3ce"
                },
                {
                    "id": "1e0afaed-d52b-47b5-bb16-291718ff9697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2518d6ce-fdf5-44ab-8e35-457c2ab391c1",
                    "LayerId": "0912e0ed-15fe-44d2-af99-49c34ece8cf3"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "440546d8-fef3-4e8c-98a6-29bb65a0b3ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "994ccc86-9868-4334-b9b8-64728e43b7a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0912e0ed-15fe-44d2-af99-49c34ece8cf3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "994ccc86-9868-4334-b9b8-64728e43b7a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}