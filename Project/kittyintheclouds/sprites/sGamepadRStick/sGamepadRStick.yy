{
    "id": "d07d8d8b-30fa-4a87-adf5-a8e766850b1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadRStick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 58,
    "bbox_right": 67,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51488517-7336-4681-8571-aed2dda96ea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07d8d8b-30fa-4a87-adf5-a8e766850b1f",
            "compositeImage": {
                "id": "beaf529c-c137-4559-a18a-2bc4ba17b1c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51488517-7336-4681-8571-aed2dda96ea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae31c48b-0771-467c-bfe1-fe17821a641e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51488517-7336-4681-8571-aed2dda96ea4",
                    "LayerId": "62488fd6-718c-4d66-be7f-10f35d4f170c"
                },
                {
                    "id": "472fa285-d496-4225-9667-793194d5ab3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51488517-7336-4681-8571-aed2dda96ea4",
                    "LayerId": "dbd4b5b8-5a0c-47c1-9f67-1ad4f0e8dec9"
                }
            ]
        },
        {
            "id": "6dde5a5d-a032-43cf-a501-d18919ae2563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07d8d8b-30fa-4a87-adf5-a8e766850b1f",
            "compositeImage": {
                "id": "db2f0186-287d-4b31-9e7d-f89e94f8c25d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dde5a5d-a032-43cf-a501-d18919ae2563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "372a8250-c556-4390-9d1b-8f5db061776f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dde5a5d-a032-43cf-a501-d18919ae2563",
                    "LayerId": "62488fd6-718c-4d66-be7f-10f35d4f170c"
                },
                {
                    "id": "65614cb3-1c59-497b-9341-a0de14a9377f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dde5a5d-a032-43cf-a501-d18919ae2563",
                    "LayerId": "dbd4b5b8-5a0c-47c1-9f67-1ad4f0e8dec9"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "62488fd6-718c-4d66-be7f-10f35d4f170c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d07d8d8b-30fa-4a87-adf5-a8e766850b1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "dbd4b5b8-5a0c-47c1-9f67-1ad4f0e8dec9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d07d8d8b-30fa-4a87-adf5-a8e766850b1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}