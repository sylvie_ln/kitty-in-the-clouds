{
    "id": "35174c13-a262-40cc-9886-232fe7a6efc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIzzy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0327b36-4f02-46f2-93c7-0a2243347749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35174c13-a262-40cc-9886-232fe7a6efc5",
            "compositeImage": {
                "id": "055cc478-5c39-4220-9c62-be5e673b6e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0327b36-4f02-46f2-93c7-0a2243347749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02690da0-55ab-4317-8cc3-20216174a73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0327b36-4f02-46f2-93c7-0a2243347749",
                    "LayerId": "e357bdd4-a92c-4cb3-a617-f012a4de54e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e357bdd4-a92c-4cb3-a617-f012a4de54e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35174c13-a262-40cc-9886-232fe7a6efc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}