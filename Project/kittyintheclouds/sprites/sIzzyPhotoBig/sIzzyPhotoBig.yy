{
    "id": "9c4f5e9f-fa34-4388-af77-5b8316919eb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIzzyPhotoBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45d25038-7d07-4daa-abbf-2b22dee018d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c4f5e9f-fa34-4388-af77-5b8316919eb9",
            "compositeImage": {
                "id": "68ff1f4e-b382-4106-84ea-e1bbcc86f1de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d25038-7d07-4daa-abbf-2b22dee018d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05d5c272-db04-4f83-8d67-ac531c5ac9f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d25038-7d07-4daa-abbf-2b22dee018d1",
                    "LayerId": "6875ad25-b015-4037-89c9-0783b25fb51b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "6875ad25-b015-4037-89c9-0783b25fb51b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c4f5e9f-fa34-4388-af77-5b8316919eb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 384
}