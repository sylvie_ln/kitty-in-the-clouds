for(var i=-1;i<=1;i++) {
    for(var j=-1;j<=1;j++) {
        if intersect_rect(
        x+(room_width*i)-abs(sprite_xoffset),y+(room_height*j)-abs(sprite_yoffset),
        x+(room_width*i)-abs(sprite_xoffset)+abs(sprite_width)-1,
        y+(room_height*j)-abs(sprite_yoffset)+abs(sprite_height)-1,
        0,0,room_width-1,room_height-1) >= 1 {
            draw_sprite_ext(sprite_index,image_index,
            x+room_width*i,y+room_height*j,
            image_xscale,image_yscale,image_angle,image_blend,image_alpha);
        }
    }
}

