///@param blend
///@param alpha
var prev_image_alpha = image_alpha;
var prev_image_blend = image_blend;
image_alpha = argument[1];
image_blend = argument[0];
if argument_count >= 4 {
    draw_outline(round(argument[2]),round(argument[3]));
} else {
    draw_outline();
}
image_alpha = prev_image_alpha;
image_blend = prev_image_blend;