var map = argument[1];
var type = argument[2]

if argument[0] == "save" {
	global_script = set_map_global;
	key_script = save_keys;
}
if argument[0] == "load" {
	global_script = get_map_global;
	key_script = load_keys;
}

if type == "game" {
	//script_execute(global_script,"something",map);
} else if type == "options" {
	script_execute(global_script,"scale",map);
	script_execute(global_script,"volume",map);
	script_execute(key_script,map);
}

return map;