// Like string_copy, but the third argument is the position of the last character to copy,
// rather than the number of characters to copy.
return string_copy(argument[0],argument[1],argument[2]-argument[1]+1);