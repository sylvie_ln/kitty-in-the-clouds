for(var i=16; i>=1; i--) {
	var w = global.view_width*i;
	var h = global.view_height*i;
	if w <= display_get_width() and h <= display_get_height() {
		global.max_scale = i;	
		break;
	}
}
return i;