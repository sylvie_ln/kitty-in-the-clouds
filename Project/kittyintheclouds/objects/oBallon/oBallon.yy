{
    "id": "3e98944e-f887-4aa2-b829-8404c64f870b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBallon",
    "eventList": [
        {
            "id": "5b25d334-278e-488a-9dbf-f475aad2308c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e98944e-f887-4aa2-b829-8404c64f870b"
        },
        {
            "id": "5572e39b-ddc2-40d1-8514-16ef18cc22ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3e98944e-f887-4aa2-b829-8404c64f870b"
        },
        {
            "id": "94bfadd5-6031-4b3d-b9f9-849e60e96bbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3e98944e-f887-4aa2-b829-8404c64f870b"
        },
        {
            "id": "fdb600d1-0601-442c-b87b-77caa6c8afb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b6a3642e-4daf-4199-8555-ebe3050605c9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3e98944e-f887-4aa2-b829-8404c64f870b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "01b5157e-72c5-4cc6-ab7b-a683ec43490f",
    "visible": true
}