draw_set_alpha(1-((time mod interval)/(interval)));
if !start or time < interval {
	draw_set_color(c_white);
	draw_set_font(global.sylvie_font_bold_2x);
	draw_text_centered(room_width div 2,4,"Kittys in the Clouds");
	draw_set_font(global.sylvie_font_bold);
	draw_text_centered(room_width div 2,40,"Click or tap to start");
	if count > 6 {
		draw_text_centered(room_width div 2,64,"This game requires audio to play.\nYour browser seems to be blocking the audio.");
	}
} else {
	draw_set_color(c_white);
	draw_set_font(global.sylvie_font_bold);
	if time < interval*2 {
		draw_text_centered(room_width div 2, 4, "Click or tap to jump and move");	
	}
	for(var i=0; i<9; i++) {
		if time >= s[i] and time < s[i+1] {
			draw_text_centered(room_width div 2,4,t[i]);	
		}
	}
}
draw_set_alpha(1);