if !start {
	if input_tap_pressed() {
		audio_play_sound(bgmClouds,100,true);
		if audio_is_playing(bgmClouds) {
			start = true;		
		} else {
			alarm[0] = 10;
		}
	}
} else {
	if time > interval*10 and !cutscene {
		cutscene = true;
		instance_create_depth(room_width,oIzzy.ground,depth,oChuchino);
	}
	if time > interval*11 and !fade {
		fade = true;
		audio_sound_gain(bgmClouds,0,8000);	
	}
	if time > interval*12 and fade {
		audio_stop_all();
		room_goto_next();
		exit;
	}
	time += delta_time;
}
stepc++;
if (time > s[4]-(interval div 8) and time < s[5])
or (time > s[8]-(interval div 8) and time < s[9]){
	with oBallon { acc = 0.25; }
}
if time > s[0]-(interval div 8) and time < s[4]-(interval div 8) {
	if stepc mod (room_speed*2) == 0 {
		instance_create_depth(room_width+8,irandom_range(64,128),depth+1,oBallon);
	}
} else if time > s[6]-(interval div 8) and time < s[8]-(interval div 8) {
	if stepc mod (room_speed div 2) == 0 {
		var ballon = instance_create_depth(room_width+8,irandom_range(64,128),depth+1,oBallon);
		ballon.hsp *= 2;
	}
}