{
    "id": "7b3b95d1-6a6f-4cd1-b18f-54b63c9bc0c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oControl",
    "eventList": [
        {
            "id": "c8cd8312-4b0d-4f7e-841a-9b6e0a025115",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7b3b95d1-6a6f-4cd1-b18f-54b63c9bc0c9"
        },
        {
            "id": "10193183-27af-403b-9b97-d929f25b8bd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7b3b95d1-6a6f-4cd1-b18f-54b63c9bc0c9"
        },
        {
            "id": "24f9645d-5967-41ba-9d72-113f2477d49e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7b3b95d1-6a6f-4cd1-b18f-54b63c9bc0c9"
        },
        {
            "id": "9697fa18-f766-4387-ae16-1f2a79059d01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "7b3b95d1-6a6f-4cd1-b18f-54b63c9bc0c9"
        },
        {
            "id": "48766ddf-ca5f-444a-851e-f376af116a9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7b3b95d1-6a6f-4cd1-b18f-54b63c9bc0c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}