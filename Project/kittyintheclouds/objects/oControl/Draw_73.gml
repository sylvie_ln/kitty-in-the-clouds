if fade {
	if time > interval*12 {
		draw_set_alpha(1);
	} else {
		draw_set_alpha(((time mod interval))/(interval));
	}
	draw_set_color(c_black);
	draw_rectangle(0,0,room_width,room_height,false);
}