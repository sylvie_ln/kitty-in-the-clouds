{
    "id": "caa4aeba-8b11-4a1c-9870-ed65d54c3e6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPop",
    "eventList": [
        {
            "id": "e3460abe-c9c8-48f0-b99e-e6ba162e16a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "caa4aeba-8b11-4a1c-9870-ed65d54c3e6d"
        },
        {
            "id": "17ec3b77-03af-4bbd-84d4-e558ed184d7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "caa4aeba-8b11-4a1c-9870-ed65d54c3e6d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4655f694-dc9c-43a5-a4fc-98dd8518c1ec",
    "visible": true
}