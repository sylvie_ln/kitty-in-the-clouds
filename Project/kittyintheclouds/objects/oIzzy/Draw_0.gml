draw_set_alpha(1);
if !oControl.cutscene {
	draw_outline_ext(c_white,0.5,last_tap_x,ground);
	if counter > 0 {
		draw_set_font(global.sylvie_font_bold);
		draw_set_color(c_white);
		draw_text_centered(x,y-25,string(counter));
		draw_text_centered(x,y-23,string(counter));
		draw_text_centered(x+1,y-24,string(counter));
		draw_text_centered(x-1,y-24,string(counter));
		draw_set_color(col);
		draw_text_centered(x,y-24,string(counter));
	}
}
draw_outline_ext(col,1);
draw_self();