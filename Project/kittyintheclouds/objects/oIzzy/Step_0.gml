var mx = mouse_x;
var my = mouse_y;
var left = point_in_rectangle(mx,my,0,0,(room_width div 2)-1,room_height);
var right = point_in_rectangle(mx,my,(room_width div 2),0,room_width,room_height);
var tap = input_tap_pressed();
var hold = input_tap_held();
var release = input_tap_released();
var on_ground = (y == ground);

if y < ground {
	vsp += grav;	
} 

if !oControl.cutscene {
	if tap {
		last_tap_x = mx;
		last_tap_y = my;
		if on_ground {
			vsp	= -jmp;
			audio_sound_pitch(sfxMeow,random_range(0.9,1.1));
			audio_play_sound(sfxMeow,10,false);
		}
	}
} else {
	spd = 1;
	last_tap_x = room_width div 2;
	if instance_exists(oChuchino) {
		if oChuchino.hspeed > 0 {
			last_tap_x = oChuchino.x;	
		}
	}
	last_tap_y = ground;
}

hsp = 0;
if x <= last_tap_x {
	hsp = spd;
	if x+spd > last_tap_x {
		hsp = 0;
		x = last_tap_x
	}
}
if x >= last_tap_x {
	hsp = -spd;	
	if x-spd < last_tap_x{
		hsp = 0;
		x = last_tap_x;
	}
}

x += hsp;
y += vsp;

if !oControl.cutscene {
	if bbox_left < 0 {
		x = abs(x-bbox_left);	
	}
	if bbox_right >= room_width {
		x = room_width-abs(x-bbox_right);	
	}
}
if y >= ground {
	y = ground;	
	counter = 0;
}