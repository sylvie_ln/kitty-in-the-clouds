{
    "id": "4d67c2d6-6be1-40e1-9c12-514a17db11f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChuchino",
    "eventList": [
        {
            "id": "6a2552c5-e580-41dc-a6da-843f6a08315d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4d67c2d6-6be1-40e1-9c12-514a17db11f7"
        },
        {
            "id": "cab4d85e-32a9-4d2e-b6e7-ad181c8fbaa5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d67c2d6-6be1-40e1-9c12-514a17db11f7"
        },
        {
            "id": "f850194e-8a2c-4c8a-acde-48c6a11baddf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4d67c2d6-6be1-40e1-9c12-514a17db11f7"
        },
        {
            "id": "edd7a910-070d-44a0-8294-a466cfa92073",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4d67c2d6-6be1-40e1-9c12-514a17db11f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "33746b64-3944-43dc-baf9-75616078f0cf",
    "visible": true
}